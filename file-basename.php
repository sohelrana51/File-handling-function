<!DOCTYPE html>
<html>
<head>
    <title>PHP File Handling</title>

    <style>
        .phpcoding {
            width:900px;
            margin:0 auto;
            background: #dddddd;
        }
        .header-option, .footer-option {
            background: #444444;
            color: #ffffff;
            text-align: center;
            padding:20px;
        }
        .maincontent {
            min-height:400px;
            padding:20px;
        }
        .header-option, .footer-option {
            margin:0;
        }
    </style>
</head>
<body>

    <section class="phpcoding">
        <div class="header-option">
            <h2>PHP Fundamentals</h2>
        </div>

        <div class="maincontent">
            <?php
                $path = "/File-Handling/Test.txt";
                echo basename($path,".txt");
            ?>


        </div>

        <div class="footer-option">
            <h2>www.makpie.com</h2>
        </div>

    </section>

</body>
</html>